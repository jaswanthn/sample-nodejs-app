const express = require('express');
const path = require('path');
const PORT = process.env.PORT || 5000;
const cool = require('cool-ascii-faces');
const mongoDB = require('mongodb');
const bodyParser = require('body-parser');
const CONTACTS_COLLECTION = 'contacts';
let db;

const app = express();
app.use(bodyParser.json())
  .use(express.static(path.join(__dirname, 'public')))
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'ejs')
  .get('/', (req, res) => res.render('pages/index'))
  .get('/cool', (req, res) => res.send(cool()))
  .get('/times', (req, res) => { res.send(showTimes()) })
  .listen(PORT, () => console.log(`Listening on ${PORT}`))

showTimes = () => {
  let result = '';
  const times = process.env.TIMES || 5
  for (i = 0; i < times; i++) {
    result += i + ''
  }
  return result;
}

mongoDB.MongoClient.connect(process.env.MONGODB_URI || "mongodb://localhost:27017/test", (err, client) => {
  if (err) {
    console.log('error in connection', err);
    process.exit(1);
  }

  db = client.db();
  console.log('database connection is ready');
})

// CONTACTS API ROUTES BELOW

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({ "error": message });
}

/*  "/api/contacts"
 *    GET: finds all contacts
 *    POST: creates a new contact
 */

app.get('/api/contacts', function (req, res) {
  db.collection(CONTACTS_COLLECTION).find({}).toArray(function (err, docs) {
    if (err) {
      handleError(res, err.message, "Failed to get contacts.");
    } else {
      res.status(200).json(docs);
    }
  });
});

app.post('/api/contacts', function (req, res) {
  var newContact = req.body;
  newContact.createDate = new Date();

  if (!req.body.name) {
    handleError(res, "Invalid user input", "Must provide a name.", 400);
  } else {
    db.collection(CONTACTS_COLLECTION).insertOne(newContact, function (err, doc) {
      if (err) {
        handleError(res, err.message, "Failed to create new contact.");
      } else {
        res.status(201).json(doc.ops[0]);
      }
    });
  }
});